﻿namespace Updater2
{
    partial class Updater
    {
        /// <summary>
        /// Erforderliche Designervariable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Verwendete Ressourcen bereinigen.
        /// </summary>
        /// <param name="disposing">True, wenn verwaltete Ressourcen gelöscht werden sollen; andernfalls False.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Vom Windows Form-Designer generierter Code

        /// <summary>
        /// Erforderliche Methode für die Designerunterstützung.
        /// Der Inhalt der Methode darf nicht mit dem Code-Editor geändert werden.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Updater));
            this.txtbox = new System.Windows.Forms.RichTextBox();
            this.cmdGetPort = new System.Windows.Forms.Button();
            this.txtPort = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.linkURL = new System.Windows.Forms.LinkLabel();
            this.cmdCheckUpdate = new System.Windows.Forms.Button();
            this.lblUpdate = new System.Windows.Forms.LinkLabel();
            this.txtFileBox = new System.Windows.Forms.RichTextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.lblVELOpath = new System.Windows.Forms.LinkLabel();
            this.label5 = new System.Windows.Forms.Label();
            this.lblJASPERpath = new System.Windows.Forms.LinkLabel();
            this.label6 = new System.Windows.Forms.Label();
            this.chkMan = new System.Windows.Forms.CheckBox();
            this.cmdStart = new System.Windows.Forms.Button();
            this.cmdSQL = new System.Windows.Forms.Button();
            this.lblServoyPath = new System.Windows.Forms.LinkLabel();
            this.label7 = new System.Windows.Forms.Label();
            this.lblDATpath = new System.Windows.Forms.LinkLabel();
            this.label8 = new System.Windows.Forms.Label();
            this.cmdRepoPaths = new System.Windows.Forms.Button();
            this.cmdUpdate = new System.Windows.Forms.Button();
            this.cmdRestartServer = new System.Windows.Forms.Button();
            this.cmdCopyFiles = new System.Windows.Forms.Button();
            this.lblStatus = new System.Windows.Forms.LinkLabel();
            this.cmdDBbackup = new System.Windows.Forms.Button();
            this.panWait = new System.Windows.Forms.Panel();
            this.prgBar = new System.Windows.Forms.ProgressBar();
            this.panelInformation = new System.Windows.Forms.GroupBox();
            this.cmdClose = new System.Windows.Forms.Button();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.panWait.SuspendLayout();
            this.panelInformation.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // txtbox
            // 
            this.txtbox.BackColor = System.Drawing.Color.White;
            this.txtbox.Location = new System.Drawing.Point(327, 214);
            this.txtbox.Name = "txtbox";
            this.txtbox.ReadOnly = true;
            this.txtbox.Size = new System.Drawing.Size(305, 198);
            this.txtbox.TabIndex = 0;
            this.txtbox.Text = "Start";
            // 
            // cmdGetPort
            // 
            this.cmdGetPort.Enabled = false;
            this.cmdGetPort.Location = new System.Drawing.Point(202, 19);
            this.cmdGetPort.Name = "cmdGetPort";
            this.cmdGetPort.Size = new System.Drawing.Size(74, 23);
            this.cmdGetPort.TabIndex = 2;
            this.cmdGetPort.Text = "Port prüfen";
            this.cmdGetPort.UseVisualStyleBackColor = true;
            this.cmdGetPort.Visible = false;
            this.cmdGetPort.Click += new System.EventHandler(this.button2_Click);
            // 
            // txtPort
            // 
            this.txtPort.BackColor = System.Drawing.Color.White;
            this.txtPort.Location = new System.Drawing.Point(79, 21);
            this.txtPort.Name = "txtPort";
            this.txtPort.ReadOnly = true;
            this.txtPort.Size = new System.Drawing.Size(49, 20);
            this.txtPort.TabIndex = 3;
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(330, 200);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(48, 13);
            this.label1.TabIndex = 4;
            this.label1.Text = "Protokoll";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(48, 24);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(26, 13);
            this.label2.TabIndex = 5;
            this.label2.Text = "Port";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(9, 50);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(65, 13);
            this.label3.TabIndex = 6;
            this.label3.Text = "Servoy-URL";
            this.label3.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // linkURL
            // 
            this.linkURL.AutoSize = true;
            this.linkURL.Location = new System.Drawing.Point(79, 50);
            this.linkURL.Name = "linkURL";
            this.linkURL.Size = new System.Drawing.Size(78, 13);
            this.linkURL.TabIndex = 7;
            this.linkURL.TabStop = true;
            this.linkURL.Text = "SERVOY LINK";
            // 
            // cmdCheckUpdate
            // 
            this.cmdCheckUpdate.Enabled = false;
            this.cmdCheckUpdate.Location = new System.Drawing.Point(1058, 207);
            this.cmdCheckUpdate.Name = "cmdCheckUpdate";
            this.cmdCheckUpdate.Size = new System.Drawing.Size(135, 23);
            this.cmdCheckUpdate.TabIndex = 8;
            this.cmdCheckUpdate.Text = "Check for Update";
            this.cmdCheckUpdate.UseVisualStyleBackColor = true;
            this.cmdCheckUpdate.Visible = false;
            this.cmdCheckUpdate.Click += new System.EventHandler(this.button3_Click);
            // 
            // lblUpdate
            // 
            this.lblUpdate.AutoSize = true;
            this.lblUpdate.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblUpdate.Location = new System.Drawing.Point(963, 11);
            this.lblUpdate.Name = "lblUpdate";
            this.lblUpdate.Size = new System.Drawing.Size(230, 25);
            this.lblUpdate.TabIndex = 9;
            this.lblUpdate.TabStop = true;
            this.lblUpdate.Text = "Update nicht verfügbar";
            this.lblUpdate.Visible = false;
            // 
            // txtFileBox
            // 
            this.txtFileBox.BackColor = System.Drawing.Color.White;
            this.txtFileBox.Location = new System.Drawing.Point(12, 214);
            this.txtFileBox.Name = "txtFileBox";
            this.txtFileBox.ReadOnly = true;
            this.txtFileBox.Size = new System.Drawing.Size(305, 198);
            this.txtFileBox.TabIndex = 10;
            this.txtFileBox.Text = "Keine Files gelesen";
            // 
            // label4
            // 
            this.label4.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(15, 200);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(33, 13);
            this.label4.TabIndex = 11;
            this.label4.Text = "Inhalt";
            // 
            // lblVELOpath
            // 
            this.lblVELOpath.AutoSize = true;
            this.lblVELOpath.Location = new System.Drawing.Point(79, 89);
            this.lblVELOpath.Name = "lblVELOpath";
            this.lblVELOpath.Size = new System.Drawing.Size(38, 13);
            this.lblVELOpath.TabIndex = 14;
            this.lblVELOpath.TabStop = true;
            this.lblVELOpath.Text = "Path...";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(3, 88);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(71, 13);
            this.label5.TabIndex = 13;
            this.label5.Text = "Druck-Pfad V";
            this.label5.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // lblJASPERpath
            // 
            this.lblJASPERpath.AutoSize = true;
            this.lblJASPERpath.Location = new System.Drawing.Point(79, 108);
            this.lblJASPERpath.Name = "lblJASPERpath";
            this.lblJASPERpath.Size = new System.Drawing.Size(38, 13);
            this.lblJASPERpath.TabIndex = 16;
            this.lblJASPERpath.TabStop = true;
            this.lblJASPERpath.Text = "Path...";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(5, 107);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(69, 13);
            this.label6.TabIndex = 15;
            this.label6.Text = "Druck-Pfad J";
            this.label6.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // chkMan
            // 
            this.chkMan.AutoSize = true;
            this.chkMan.Location = new System.Drawing.Point(134, 23);
            this.chkMan.Name = "chkMan";
            this.chkMan.Size = new System.Drawing.Size(62, 17);
            this.chkMan.TabIndex = 20;
            this.chkMan.Text = "manuell";
            this.chkMan.UseVisualStyleBackColor = true;
            this.chkMan.CheckedChanged += new System.EventHandler(this.chkMan_CheckedChanged);
            // 
            // cmdStart
            // 
            this.cmdStart.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmdStart.Location = new System.Drawing.Point(12, 419);
            this.cmdStart.Name = "cmdStart";
            this.cmdStart.Size = new System.Drawing.Size(305, 52);
            this.cmdStart.TabIndex = 22;
            this.cmdStart.Text = "Update starten";
            this.cmdStart.UseVisualStyleBackColor = true;
            this.cmdStart.Click += new System.EventHandler(this.cmdStart_Click);
            // 
            // cmdSQL
            // 
            this.cmdSQL.Location = new System.Drawing.Point(1058, 39);
            this.cmdSQL.Name = "cmdSQL";
            this.cmdSQL.Size = new System.Drawing.Size(135, 23);
            this.cmdSQL.TabIndex = 30;
            this.cmdSQL.Text = "execute SQL";
            this.cmdSQL.UseVisualStyleBackColor = true;
            this.cmdSQL.Visible = false;
            this.cmdSQL.Click += new System.EventHandler(this.button1_Click_1);
            // 
            // lblServoyPath
            // 
            this.lblServoyPath.AutoSize = true;
            this.lblServoyPath.Location = new System.Drawing.Point(79, 69);
            this.lblServoyPath.Name = "lblServoyPath";
            this.lblServoyPath.Size = new System.Drawing.Size(38, 13);
            this.lblServoyPath.TabIndex = 29;
            this.lblServoyPath.TabStop = true;
            this.lblServoyPath.Text = "Path...";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(9, 69);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(65, 13);
            this.label7.TabIndex = 28;
            this.label7.Text = "Servoy-Pfad";
            this.label7.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // lblDATpath
            // 
            this.lblDATpath.AutoSize = true;
            this.lblDATpath.Location = new System.Drawing.Point(79, 126);
            this.lblDATpath.Name = "lblDATpath";
            this.lblDATpath.Size = new System.Drawing.Size(38, 13);
            this.lblDATpath.TabIndex = 32;
            this.lblDATpath.TabStop = true;
            this.lblDATpath.Text = "Path...";
            this.lblDATpath.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabel1_LinkClicked);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(14, 126);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(60, 13);
            this.label8.TabIndex = 31;
            this.label8.Text = "Datenbank";
            this.label8.TextAlign = System.Drawing.ContentAlignment.TopRight;
            this.label8.Click += new System.EventHandler(this.label8_Click);
            // 
            // cmdRepoPaths
            // 
            this.cmdRepoPaths.Enabled = false;
            this.cmdRepoPaths.Location = new System.Drawing.Point(1058, 160);
            this.cmdRepoPaths.Name = "cmdRepoPaths";
            this.cmdRepoPaths.Size = new System.Drawing.Size(135, 23);
            this.cmdRepoPaths.TabIndex = 12;
            this.cmdRepoPaths.Text = "Get Repo Paths";
            this.cmdRepoPaths.UseVisualStyleBackColor = true;
            this.cmdRepoPaths.Visible = false;
            this.cmdRepoPaths.Click += new System.EventHandler(this.cmdRepoPaths_Click);
            // 
            // cmdUpdate
            // 
            this.cmdUpdate.Enabled = false;
            this.cmdUpdate.Location = new System.Drawing.Point(1058, 294);
            this.cmdUpdate.Name = "cmdUpdate";
            this.cmdUpdate.Size = new System.Drawing.Size(135, 23);
            this.cmdUpdate.TabIndex = 19;
            this.cmdUpdate.Text = "Servoy Update";
            this.cmdUpdate.UseVisualStyleBackColor = true;
            this.cmdUpdate.Visible = false;
            this.cmdUpdate.Click += new System.EventHandler(this.cmdUpdate_Click);
            // 
            // cmdRestartServer
            // 
            this.cmdRestartServer.Enabled = false;
            this.cmdRestartServer.Location = new System.Drawing.Point(1058, 323);
            this.cmdRestartServer.Name = "cmdRestartServer";
            this.cmdRestartServer.Size = new System.Drawing.Size(135, 23);
            this.cmdRestartServer.TabIndex = 1;
            this.cmdRestartServer.Text = "Server Restart";
            this.cmdRestartServer.UseVisualStyleBackColor = true;
            this.cmdRestartServer.Visible = false;
            this.cmdRestartServer.Click += new System.EventHandler(this.button1_Click);
            // 
            // cmdCopyFiles
            // 
            this.cmdCopyFiles.Enabled = false;
            this.cmdCopyFiles.Location = new System.Drawing.Point(1058, 265);
            this.cmdCopyFiles.Name = "cmdCopyFiles";
            this.cmdCopyFiles.Size = new System.Drawing.Size(135, 23);
            this.cmdCopyFiles.TabIndex = 18;
            this.cmdCopyFiles.Text = "Copy Files";
            this.cmdCopyFiles.UseVisualStyleBackColor = true;
            this.cmdCopyFiles.Visible = false;
            this.cmdCopyFiles.Click += new System.EventHandler(this.cmdCopyFiles_Click);
            // 
            // lblStatus
            // 
            this.lblStatus.AutoSize = true;
            this.lblStatus.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblStatus.LinkBehavior = System.Windows.Forms.LinkBehavior.HoverUnderline;
            this.lblStatus.Location = new System.Drawing.Point(42, 181);
            this.lblStatus.Name = "lblStatus";
            this.lblStatus.Size = new System.Drawing.Size(212, 37);
            this.lblStatus.TabIndex = 22;
            this.lblStatus.TabStop = true;
            this.lblStatus.Text = "Bitte warten...";
            this.lblStatus.Visible = false;
            // 
            // cmdDBbackup
            // 
            this.cmdDBbackup.Location = new System.Drawing.Point(1058, 236);
            this.cmdDBbackup.Name = "cmdDBbackup";
            this.cmdDBbackup.Size = new System.Drawing.Size(135, 23);
            this.cmdDBbackup.TabIndex = 33;
            this.cmdDBbackup.Text = "Datensicherung CL";
            this.cmdDBbackup.UseVisualStyleBackColor = true;
            this.cmdDBbackup.Visible = false;
            this.cmdDBbackup.Click += new System.EventHandler(this.button1_Click_2);
            // 
            // panWait
            // 
            this.panWait.Controls.Add(this.prgBar);
            this.panWait.Controls.Add(this.lblStatus);
            this.panWait.Location = new System.Drawing.Point(762, 352);
            this.panWait.Name = "panWait";
            this.panWait.Size = new System.Drawing.Size(620, 459);
            this.panWait.TabIndex = 23;
            this.panWait.Visible = false;
            // 
            // prgBar
            // 
            this.prgBar.Location = new System.Drawing.Point(49, 222);
            this.prgBar.Name = "prgBar";
            this.prgBar.Size = new System.Drawing.Size(521, 23);
            this.prgBar.TabIndex = 23;
            this.prgBar.Visible = false;
            // 
            // panelInformation
            // 
            this.panelInformation.Controls.Add(this.chkMan);
            this.panelInformation.Controls.Add(this.txtPort);
            this.panelInformation.Controls.Add(this.lblDATpath);
            this.panelInformation.Controls.Add(this.cmdGetPort);
            this.panelInformation.Controls.Add(this.label2);
            this.panelInformation.Controls.Add(this.label8);
            this.panelInformation.Controls.Add(this.label3);
            this.panelInformation.Controls.Add(this.linkURL);
            this.panelInformation.Controls.Add(this.label5);
            this.panelInformation.Controls.Add(this.lblVELOpath);
            this.panelInformation.Controls.Add(this.lblServoyPath);
            this.panelInformation.Controls.Add(this.label6);
            this.panelInformation.Controls.Add(this.lblJASPERpath);
            this.panelInformation.Controls.Add(this.label7);
            this.panelInformation.Location = new System.Drawing.Point(12, 12);
            this.panelInformation.Name = "panelInformation";
            this.panelInformation.Size = new System.Drawing.Size(305, 178);
            this.panelInformation.TabIndex = 34;
            this.panelInformation.TabStop = false;
            this.panelInformation.Text = "Information";
            // 
            // cmdClose
            // 
            this.cmdClose.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmdClose.Location = new System.Drawing.Point(327, 419);
            this.cmdClose.Name = "cmdClose";
            this.cmdClose.Size = new System.Drawing.Size(305, 52);
            this.cmdClose.TabIndex = 35;
            this.cmdClose.Text = "Schließen";
            this.cmdClose.UseVisualStyleBackColor = true;
            this.cmdClose.Click += new System.EventHandler(this.cmdClose_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::Updater2.Properties.Resources.hvo2go_markenzeichen_4c_verlauf;
            this.pictureBox1.InitialImage = ((System.Drawing.Image)(resources.GetObject("pictureBox1.InitialImage")));
            this.pictureBox1.Location = new System.Drawing.Point(327, 12);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(305, 178);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 36;
            this.pictureBox1.TabStop = false;
            this.pictureBox1.Click += new System.EventHandler(this.pictureBox1_Click);
            // 
            // Updater
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(1194, 617);
            this.Controls.Add(this.panWait);
            this.Controls.Add(this.cmdClose);
            this.Controls.Add(this.panelInformation);
            this.Controls.Add(this.cmdDBbackup);
            this.Controls.Add(this.cmdRepoPaths);
            this.Controls.Add(this.cmdCopyFiles);
            this.Controls.Add(this.cmdSQL);
            this.Controls.Add(this.cmdRestartServer);
            this.Controls.Add(this.cmdUpdate);
            this.Controls.Add(this.cmdStart);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.txtFileBox);
            this.Controls.Add(this.lblUpdate);
            this.Controls.Add(this.cmdCheckUpdate);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txtbox);
            this.Controls.Add(this.pictureBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "Updater";
            this.Text = "Updater 2020";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.panWait.ResumeLayout(false);
            this.panWait.PerformLayout();
            this.panelInformation.ResumeLayout(false);
            this.panelInformation.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.RichTextBox txtbox;
        private System.Windows.Forms.Button cmdGetPort;
        private System.Windows.Forms.TextBox txtPort;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.LinkLabel linkURL;
        private System.Windows.Forms.Button cmdCheckUpdate;
        private System.Windows.Forms.LinkLabel lblUpdate;
        private System.Windows.Forms.RichTextBox txtFileBox;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.LinkLabel lblVELOpath;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.LinkLabel lblJASPERpath;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.CheckBox chkMan;
        private System.Windows.Forms.Button cmdStart;
        private System.Windows.Forms.LinkLabel lblServoyPath;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Button cmdSQL;
        private System.Windows.Forms.LinkLabel lblDATpath;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Button cmdRepoPaths;
        private System.Windows.Forms.Button cmdUpdate;
        private System.Windows.Forms.Button cmdRestartServer;
        private System.Windows.Forms.Button cmdCopyFiles;
        private System.Windows.Forms.LinkLabel lblStatus;
        private System.Windows.Forms.Button cmdDBbackup;
        private System.Windows.Forms.Panel panWait;
        private System.Windows.Forms.GroupBox panelInformation;
        private System.Windows.Forms.Button cmdClose;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.ProgressBar prgBar;
    }
}

