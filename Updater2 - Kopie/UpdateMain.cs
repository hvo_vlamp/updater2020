﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Net;
using System.IO;
using RestSharp;
using RestSharp.Authenticators;
using System.Data.OleDb;
using System.IO.Compression;
using System.ServiceProcess;
using Kajabity.Tools.Java;
using System.Diagnostics;

namespace Updater2
{
    public partial class UpdateMain : Form
    {
        public string VeloPath = "FEHLER";
        public string JasperPath = "FEHLER";
        public string ServoyPath = "FEHLER";
        public string DBPath = "FEHLER";


        public UpdateMain()
        {
            InitializeComponent();
            // --- DESIGN setzen ---
            pnlDesign.Height = cmdUpdate.Height;
            pnlDesign.Top = cmdUpdate.Top;
            pnlUpdate.Visible = true;
            pnlUpdate.BringToFront();
            pnlLog.Visible = false;
            Application.DoEvents();
            // --- Status setzen ---
            SetStatus("Updater gestartet");
            UpdateLogText("Updater gestartet");
        }

        private void cmdUpdate_Click(object sender, EventArgs e)
        {
            pnlDesign.Height = cmdUpdate.Height;
            pnlDesign.Top = cmdUpdate.Top;

            pnlUpdate.Visible = true;
            pnlUpdate.BringToFront();
            pnlLog.Visible = false;
            Application.DoEvents();

        }

        private void button2_Click(object sender, EventArgs e)
        {
            pnlDesign.Height = cmdLog.Height;
            pnlDesign.Top = cmdLog.Top;

            pnlLog.Visible = true;
            pnlUpdate.Visible = false;
            pnlLog.BringToFront();
            Application.DoEvents();
        }

        private void cmdExit_Click(object sender, EventArgs e)
        {
            pnlDesign.Height = cmdExit.Height;
            pnlDesign.Top = cmdExit.Top;

            onExit();

        }

        private void onExit()
        {
            Environment.Exit(0);
        }

        // --- FORM verschieben --- 
        private bool mouseDown;
        private Point lastLocation;
        private string UpdateLog = "";

        private void UpdateMain_MouseDown(object sender, MouseEventArgs e)
        {
            mouseDown = true;
            lastLocation = e.Location;
        }

        private void UpdateMain_MouseMove(object sender, MouseEventArgs e)
        {
            if (mouseDown)
            {
                this.Location = new Point(
                    (this.Location.X - lastLocation.X) + e.X, (this.Location.Y - lastLocation.Y) + e.Y);

                this.Update();
            }
        }

        private void UpdateMain_MouseUp(object sender, MouseEventArgs e)
        {
            mouseDown = false;
        }

        private void SetStatus(string value){
            lblStatus.Text = value;
            Application.DoEvents();
        }

        private void UpdateLogText(string text)
        {
            UpdateLog += DateTime.Now.ToString() + " --- " + text + "\n";
            txtLog.Text = UpdateLog;
            Application.DoEvents();
        }

        private void cmdStartUpdate_Click(object sender, EventArgs e)
        {
            // --- 1) Dienst anhalten ---
            if(StopAppServer() == false)
            {
                return;
            }
            pic4.Visible = true;
            SetStatus("Servoy-Dienst erfolgreich gestoppt...");
            UpdateLogText("Servoy-Dienst erfolgreich gestoppt");

            // --- 2) Backup vom Servoy 8 anlegen ---
            if (BackupServoy8() == false)
            {
                return;
            }
            pic5.Visible = true;
            SetStatus("Servoy-8 Backup erfolgreich angelegt...");
            UpdateLogText("Servoy-8 Backup erfolgreich angelegt");

            // --- 3) Servoy20.zip entpacken in den SourcePfad ---
            if (copyServoy20() == false)
            {
                return;
            }
            pic6.Visible = true;
            SetStatus("Servoy-20 Server erfolgreich angelegt...");
            UpdateLogText("Servoy-20 Server erfolgreich angelegt");

            // --- 4) Config anpassen (Werte aus S8 in S20 übernehmen) ---
            if (CreateConfig() == false)
            {
                return;
            }
            pic7.Visible = true;
            SetStatus("Properties erfolgreich erstellt...");
            UpdateLogText("Properties erfolgreich erstellt");

            // --- 5) Service anpassen (Werte aus S8 in S20 übernehmen) ---
            if (createService() == false)
            {
                return;
            }
            pic11.Visible = true;
            SetStatus("Service-Config erfolgreich erstellt...");
            UpdateLogText("Service-Config erfolgreich erstellt");

            // --- 6) Server.XML anpassen (Werte aus S8 in S20 übernehmen) ---
            if (createServerXML() == false)
            {
                return;
            }
            pic12.Visible = true;
            SetStatus("Server.XML erfolgreich erstellt...");
            UpdateLogText("Sercer.XML erfolgreich erstellt");

        }

        private void UpdateMain_Load(object sender, EventArgs e)
        {
            // --- Port holen ---
            SetStatus("Prüfen Port...");
            string lcPort = getPort();
            if (lcPort == "FEHLER")
            {
                lcPort = getPortMan();
            }
            if (lcPort == "FEHLER")
            {
                return;
            }
            pic2.Visible        = true;
            Globals.Port        = lcPort;
            lblServoyPort.Text  = lcPort;
            lblServerURL.Text = Globals.Host + ":8080/";

            // --- Update prüfen ---
            SetStatus("Prüfen Update...");
            string lcCheckUpate = checkUpdate();
            if (lcCheckUpate == "FEHLER")
            {
                return;
            }
            pic1.Visible = true;

            // --- Prüfe Pfade ---
            SetStatus("Prüfe Pfade...");
            Boolean lcCheckPfade = checkPfade() ;
            if (lcCheckPfade == false)
            {
                return;
            }
            pic3.Visible = true;

            // --- GO freischalten ---
            cmdStartUpdate.Enabled = true;
            SetStatus("Bereit für Update...");

            // --- 


        }

        // --- Funktion welcher Port verwendet wird ---
        public string getPort()
        {
            string lcReturn = "FEHLER";
            string username = "update";
            string password = "update";
            string encoded = Convert.ToBase64String(Encoding.GetEncoding("ISO-8859-1").GetBytes($"{username}:{password}"));

            var client = new RestClient(Globals.Host + ":8080/servoy-admin/");
            var request = new RestRequest(Method.GET);

            client.Timeout = 2000;
            request.AddHeader("Authorization", $"Basic {encoded}");
            request.AddHeader("Content-Type", "application/x-www-form-urlencoded");
            IRestResponse response = client.Execute(request);
            HttpStatusCode statusCode = response.StatusCode;


            int numericStatusCode = (int)statusCode;

            if (numericStatusCode == 200)
            {
                lcReturn = ":8080";
                UpdateLogText("Port wurde gefunden (8080)");
            }
            else
            {   // --- Ist nicht die 8080, 8090 pingen ---
                var client2 = new RestClient(Globals.Host + ":8090/servoy-admin/");
                var request2 = new RestRequest(Method.GET);

                client2.Timeout = 2000;
                request2.AddHeader("Authorization", $"Basic {encoded}");
                request2.AddHeader("Content-Type", "application/x-www-form-urlencoded");
                IRestResponse response2 = client2.Execute(request2);
                HttpStatusCode statusCode2 = response2.StatusCode;

                int numericStatusCode2 = (int)statusCode2;
                if (numericStatusCode2 == 200)
                {
                    lcReturn = ":8090";
                    UpdateLogText("Port wurde gefunden (8090)");
                }
                else
                { // --- Ist nicht die 8080 & 8090, 9090 nun pingen, wenns nicht klappt dann manuell eingeben ---
                    var client3 = new RestClient(Globals.Host + ":9090/servoy-admin/");

                    var request3 = new RestRequest(Method.GET);

                    client3.Timeout = 2000;
                    request3.AddHeader("Authorization", $"Basic {encoded}");
                    request3.AddHeader("Content-Type", "application/x-www-form-urlencoded");
                    IRestResponse response3 = client3.Execute(request3);
                    HttpStatusCode statusCode3 = response3.StatusCode;

                    int numericStatusCode3 = (int)statusCode3;
                    if (numericStatusCode3 == 200)
                    {
                        lcReturn = ":9090";
                        UpdateLogText("Port wurde gefunden (9090)");
                    }
                    else
                    {
                        lcReturn = "FEHLER";
                        UpdateLogText("Port wurde NICHT gefunden");
                    }
                }
            }

            return lcReturn;
        }


        public string getPortMan()
        {
            string lcReturn = "FEHLER";
            string input = Microsoft.VisualBasic.Interaction.InputBox("Konnte den Port automatisch nicht finden!\nBitte Port eingeben", "Fehler", "8080");

            if (input.Length == 0)
            {
                lcReturn = "FEHLER";
            }
            else
            {
               
                string username = "update";
                string password = "update";
                string encoded = Convert.ToBase64String(Encoding.GetEncoding("ISO-8859-1").GetBytes($"{username}:{password}"));

                var client = new RestClient(Globals.Host + ":" + input + "/servoy-admin/");
                var request = new RestRequest(Method.GET);

                client.Timeout = 2000;
                request.AddHeader("Authorization", $"Basic {encoded}");
                request.AddHeader("Content-Type", "application/x-www-form-urlencoded");
                IRestResponse response = client.Execute(request);
                HttpStatusCode statusCode = response.StatusCode;


                int numericStatusCode = (int)statusCode;

                if (numericStatusCode == 200)
                {
                    lcReturn = ":" + input;
                    UpdateLogText("Port wurde gefunden ("+input+")");
                }
                else
                {
                    DialogResult dialogResult = MessageBox.Show("Port stimmt nicht\nErneut versuchen?", "Fehler", MessageBoxButtons.YesNo);
                    if (dialogResult == DialogResult.Yes)
                    {
                        getPortMan();
                    }
                    else if (dialogResult == DialogResult.No)
                    {
                        lcReturn = "FEHLER";
                    }
                }
            }
            return lcReturn;
        } 


        private string checkUpdate()
        {
            // --- Schauen ob ein Update bereit liegt auf C:\2goupd\UPD ---
            string lcReturn = "FEHLER";
            string lcPath = Globals.InstallPath + @"UPD\";
            if (Directory.Exists(lcPath))
            {
                DirectoryInfo d = new DirectoryInfo(lcPath);    //Assuming Test is your Folder
                FileInfo[] Files = d.GetFiles("*.*");                           //Getting Text files
                string str = "Folgende Updatedateien wurden gefunden:";
                foreach (FileInfo file in Files)
                {
                    str = str + "\n" + file.Name;
                }
                txtFileBox.Text = str;
                lcReturn = "OK";
            }
            else
            {
                MessageBox.Show("Kein Update Ordner gefunden \nBitte Installation prüfen.", "Fehler", MessageBoxButtons.OK);
            }

            return lcReturn;
        }

        private Boolean checkPfade()
        {
            var lcPort = Globals.Port;

            var lcURL = Globals.Host + lcPort + "/servoy-admin/plugin-settings";
            var lcVELO = "";
            var lcJASPER = "";
            var lcServoy = "";
            var lcDAT = "";

            string username = "update";
            string password = "update";
            string encoded = Convert.ToBase64String(Encoding.GetEncoding("ISO-8859-1").GetBytes($"{username}:{password}"));

            var client2 = new RestClient(lcURL);
            var request2 = new RestRequest(Method.GET);

            client2.Timeout = 3000;
            request2.AddHeader("Authorization", $"Basic {encoded}");
            request2.AddHeader("Content-Type", "text/html");

            IRestResponse response2 = client2.Execute(request2);

            string lcBody = response2.Content;
            
            // --- Finde VELO Pfad ---
            var lnAT = lcBody.IndexOf("=\"velocityreport.reportfolder\"");
            if (lnAT < 0)
            {
                lcVELO = "FEHLER";
                VeloPath = lcVELO;
                UpdateLogText("Velocity Pfad NICHT gefunden");
                return false;
            }

            lcVELO = lcBody.Substring(lnAT);
            lnAT = lcVELO.IndexOf("ue=\"") + 4;
            lcVELO = lcVELO.Substring(lnAT);
            lnAT = lcVELO.IndexOf("\"");
            lcVELO = lcVELO.Substring(0, lnAT);
            VeloPath = lcVELO;

            UpdateLogText("Velocity Pfad gefunden (" + lcVELO + ")");

            // --- Finde JASPER Pfad ---
            lnAT = lcBody.IndexOf("=\"directory.jasper.report\"");
            if (lnAT < 0)
            {
                lcJASPER = "FEHLER";
                JasperPath = lcJASPER;
                UpdateLogText("Jasper Pfad NICHT gefunden");
                return false;
            }

            lcJASPER = lcBody.Substring(lnAT);
            lnAT = lcJASPER.IndexOf("ue=\"") + 4;
            lcJASPER = lcJASPER.Substring(lnAT);
            lnAT = lcJASPER.IndexOf("\"");
            lcJASPER = lcJASPER.Substring(0, lnAT);
            JasperPath = lcJASPER;

            UpdateLogText("Jasper Pfad gefunden (" + lcJASPER + ")");

            // --- Finde Servoy Pfad ---
            lnAT = lcBody.IndexOf("=\"velocityreport.reportfolder\"");
            if (lnAT < 0)
            {
                lcServoy = "FEHLER";
                ServoyPath = lcServoy;
                UpdateLogText("Servoy Pfad NICHT gefunden");
                return false;
            }

            lcServoy = lcBody.Substring(lnAT);
            lnAT = lcServoy.IndexOf("ue=\"") + 4;
            lcServoy = lcServoy.Substring(lnAT);
            lnAT = lcServoy.ToLower().IndexOf("servoy");
            lcServoy = lcServoy.Substring(0, lnAT + 7);
            ServoyPath = lcServoy;

            UpdateLogText("Servoy Pfad gefunden (" + lcServoy + ")");

            lblServoyPfad.Text = lcServoy;

            // --- Finde DAT Pfad ---
            lnAT = lcBody.IndexOf("servoy.FileServerService.defaultFolder\"");
            if (lnAT < 0)
            {
                lcDAT = "FEHLER";
                DBPath = lcDAT;
                UpdateLogText("Datenbank Pfad NICHT gefunden");
                return false;
            }

            lcDAT = lcBody.Substring(lnAT);
            lnAT = lcDAT.IndexOf("ue=\"") + 4;
            lcDAT = lcDAT.Substring(lnAT);
            lnAT = lcDAT.ToLower().IndexOf("\">");
            lcDAT = lcDAT.Substring(0, lnAT) + "\\DAT\\";

            UpdateLogText("Datenbank Pfad gefunden (" + lcDAT + ")");

            lblDB.Text = lcDAT;

            return true;
        }

        private Boolean StopAppServer()
        {
            Boolean llReturn = false;

            ServiceController sc = new ServiceController("PDF24");

            if ((sc.Status.Equals(ServiceControllerStatus.Stopped)) || (sc.Status.Equals(ServiceControllerStatus.StopPending)))
            {
                llReturn = true;
            }
            else
            {
                // Stop the service if its status is not set to "Stopped".
                SetStatus("Stoppe Servoy-Dienst...");
                sc.Stop();
                sc.WaitForStatus(ServiceControllerStatus.Stopped);
                llReturn = true;
            }
            return llReturn;
        }

        private Boolean StartAppServer()
        {
            Boolean llReturn = false;

            ServiceController sc = new ServiceController("PDF24");

            if ((sc.Status.Equals(ServiceControllerStatus.Stopped)) || (sc.Status.Equals(ServiceControllerStatus.StopPending)))
            {
                // Start the service if its status is set to "Stopped".
                SetStatus("Starte Servoy-Dienst...");
                sc.Start();
                sc.WaitForStatus(ServiceControllerStatus.Running);
                llReturn = true;
            }
            else
            {
                llReturn = true;
            }
            return llReturn;
        }

        private Boolean BackupServoy8()
        {
            Boolean llReturn        = false;
            string DestinationPath  = Globals.InstallPath + @"Backup\";
            string SourcePath       = ServoyPath;


            // --- BackupOrdner erstellen falls nötig ---
            if (Directory.Exists(DestinationPath))
            {
                Directory.Delete(DestinationPath, true);
            }

            Directory.CreateDirectory(DestinationPath);



            // --- Ganzen Servoy Ordner kopieren ---
            //Now Create all of the directories

            prgBar.Maximum  = Directory.GetDirectories(SourcePath, "*", SearchOption.AllDirectories).Length;
            prgBar.Value = 0;
            SetStatus("Erstelle Servoy 8 Backup...");
            foreach (string dirPath in Directory.GetDirectories(SourcePath, "*", SearchOption.AllDirectories))
            {
                Directory.CreateDirectory(dirPath.Replace(SourcePath, DestinationPath));
                prgBar.Value += 1;
                Application.DoEvents();
            }


            //Copy all the files & Replaces any files with the same name
            prgBar.Maximum = Directory.GetFiles(SourcePath, "*.*", SearchOption.AllDirectories).Length;
            prgBar.Value = 0;
            SetStatus("Erstelle Servoy 8 Backup...");

            foreach (string newPath in Directory.GetFiles(SourcePath, "*.*", SearchOption.AllDirectories))
            {
                File.Copy(newPath, newPath.Replace(SourcePath, DestinationPath), true);
                prgBar.Value += 1;
                SetStatus("Backup: " + newPath);
                Application.DoEvents();
            }
            llReturn = true;
            System.Threading.Thread.Sleep(3000);

            // --- SourcePfad löschen ---
            //Directory.Move(SourcePath,SourcePath + "_8bfUpd");


            if (Directory.Exists(SourcePath))
            {
                Directory.Delete(SourcePath, true);
                Directory.CreateDirectory(SourcePath);
            }

            return llReturn;
        }

        private Boolean copyServoy20()
        {
            Boolean llReturn = false;
            SetStatus("Erstelle Servoy-20 Server...");

            // --- In den Sourcepfad die Servoy20.zip entpacken ---
            string SourcePath = ServoyPath;
            string Servoy20 = Globals.InstallPath + @"UPD\Servoy20.zip";

            if (!Directory.Exists(SourcePath))
            {
                Directory.CreateDirectory(SourcePath);
            }

            if (!File.Exists(Servoy20))
            {
                return false;
            }

            // --- Sourcepath und ZIP sind vorhanden, diese nun entpacken ---

            try
            {
                ZipFile.ExtractToDirectory(Servoy20, SourcePath);
            }
            catch 
            {
                return false;
            }


            llReturn = true;


            return llReturn;
        }

        private Boolean CreateConfig()
        {
            Boolean llReturn = false;
            string lcOldProp = File.ReadAllText(Globals.InstallPath + @"Backup\application_server\servoy.properties");
            string lcNewProp = File.ReadAllText(ServoyPath + @"\application_server\servoy.properties");
            string lcTransfer = "";
            SetStatus("Erstelle Properties...");
           
            using (StringReader reader = new StringReader(lcOldProp))
            {
                string line;
                while ((line = reader.ReadLine()) != null)
                {
                    // --- Server übernehmen ---
                    if (line.Length >= 7)
                    {
                        if (line.Substring(0, 7).ToLower() == "server.")
                        {
                            lcTransfer += line + "\r\n";
                            SetStatus("Config_Export_DB: " + line);
                            UpdateLogText("Config_Export_DB: " + line);
                        }
                    }
 
                    // --- Lizenzen übernehmen ---
                    if (line.ToLower().Contains("license."))
                    {
                        lcTransfer += line + "\r\n";
                        SetStatus("Config_Export_LIC: " + line);
                        UpdateLogText("Config_Export_LIC: " + line);
                    }
                    // --- NumberOfServers übernehmen ---
                    if (line.ToLower().Contains("servermanager.numberofservers"))
                    {
                        lcTransfer += line + "\r\n";
                        SetStatus("Config_Export_NS: " + line);
                        UpdateLogText("Config_Export_NS: " + line);
                    }
                    // --- FileService übernehmen ---
                    if (line.ToLower().Contains("servoy.fileserverservice.defaultfolder"))
                    {
                        lcTransfer += line + "\r\n";
                        SetStatus("Config_Export_FS: " + line);
                        UpdateLogText("Config_Export_FS: " + line);
                    }
                    // --- VeloRepo übernehmen ---
                    if (line.ToLower().Contains("velocityreport."))
                    {
                        lcTransfer += line + "\r\n";
                        SetStatus("Config_Export_VELO: " + line);
                        UpdateLogText("Config_Export_VELO: " + line);
                    }
                    // --- Jasper übernehmen ---
                    if (line.ToLower().Contains("jasper."))
                    {
                        lcTransfer += line + "\r\n";
                        SetStatus("Config_Export_JASPER: " + line);
                        UpdateLogText("Config_Export_JASPER: " + line);
                    }
                    // --- MailPro übernehmen ---
                    if (line.ToLower().Contains("drmaison."))
                    {
                        lcTransfer += line + "\r\n";
                        SetStatus("Config_Export_MAILPRO: " + line);
                        UpdateLogText("Config_Export_MAILPRO: " + line);
                    }
                    // --- Batchprozess übernehmen ---
                    if (line.ToLower().Contains("batchprocess."))
                    {
                        lcTransfer += line + "\r\n";
                        SetStatus("Config_Export_BATCHPRO: " + line);
                        UpdateLogText("Config_Export_BATCHPRO: " + line);
                    }
                }
            }
            // --- Wenn Transfer leer ist dann FEHLER ---
            if (lcTransfer.Length == 0)
            {
                llReturn = false;
                return llReturn;
            }

            // --- Properties 2020 Vorlage einlesen und Transfer einfügen ----
            string newProperties = lcNewProp + "\r\n" + lcTransfer;

            if (File.Exists(ServoyPath + @"\application_server\servoy.properties_template"))
            {
                File.Delete(ServoyPath + @"\application_server\servoy.properties_template");
            }

            File.Move(ServoyPath + @"\application_server\servoy.properties", ServoyPath + @"\application_server\servoy.properties_template");

            if (!File.Exists(ServoyPath + @"\application_server\servoy.properties"))
            {
                File.WriteAllText(ServoyPath + @"\application_server\servoy.properties", newProperties);
            }
            else
            {
                File.AppendAllText(ServoyPath + @"\application_server\servoy.properties", newProperties);
            }

            llReturn = true;
            
            return llReturn;
        }


        private Boolean createService()
        {
            Boolean llReturn = false;

            // --- Wie bei Properties die Service.config anpassen und speichern ---
            string lcOldService         = File.ReadAllText(Globals.InstallPath + @"Backup\application_server\service\wrapper.conf");
            string lcNewService         = File.ReadAllText(ServoyPath + @"\application_server\service\wrapper.conf");
            string lcTransferService    = "";
            SetStatus("Erstelle Service-Config...");

            using (StringReader reader = new StringReader(lcOldService))
            {
                string line;
                while ((line = reader.ReadLine()) != null)
                {
                    // --- Pfadeinstellungen rauslesen ---
                    if (line.ToLower().Contains("wrapper.java.library.path"))
                    {
                        lcTransferService += line + "\r\n";
                        UpdateLogText("Service_Export_PATH: " + line);
                    }

                    if (line.ToLower().Contains("wrapper.java.additional"))
                    {
                        lcTransferService += line + "\r\n";
                        UpdateLogText("Service_Export_PATH: " + line);
                    }

                    if (line.ToLower().Contains("wrapper.working.dir"))
                    {
                        lcTransferService += line + "\r\n";
                        UpdateLogText("Service_Export_PATH: " + line);
                    }

                    if (line.ToLower().Contains("wrapper.java.initmemory"))
                    {
                        lcTransferService += line + "\r\n";
                        UpdateLogText("Service_Export_INITMEM: " + line);
                    }

                    if (line.ToLower().Contains("wrapper.java.maxmemory"))
                    {
                        lcTransferService += line + "\r\n";
                        UpdateLogText("Service_Export_MAXMEM: " + line);
                    }
                }
            }

            // --- Wenn Transfer leer ist dann FEHLER ---
            if (lcTransferService.Length == 0)
            {
                llReturn = false;
                return llReturn;
            }

            // --- Properties 2020 Vorlage einlesen und Transfer einfügen ----
            string newProperties = lcNewService + "\r\n" + lcTransferService;

            if (File.Exists(ServoyPath + @"\application_server\service\wrapper.conf_template"))
            {
                File.Delete(ServoyPath + @"\application_server\service\wrapper.conf_template");
            }

            File.Move(ServoyPath + @"\application_server\service\wrapper.conf", ServoyPath + @"\application_server\service\wrapper.conf_template");

            if (!File.Exists(ServoyPath + @"\application_server\service\wrapper.conf"))
            {
                File.WriteAllText(ServoyPath + @"\application_server\service\wrapper.conf", newProperties);
            }
            else
            {
                File.AppendAllText(ServoyPath + @"\application_server\service\wrapper.conf", newProperties);
            }

            llReturn = true;

            return llReturn;
        }

        private Boolean createServerXML()
        {
            Boolean llReturn = false;

            // --- SERVER.XML aus Servoy8 in Servoy20 kopieren ---
            string lcOldXML = Globals.InstallPath + @"Backup\application_server\server\conf\server.xml";
            string lcNewXML = ServoyPath + @"\application_server\server\conf\server.xml";
              SetStatus("Erstelle Server-XML...");

            if (File.Exists(lcNewXML) == true)
            {
                File.Delete(lcNewXML);
            }

            File.Copy(lcOldXML, lcNewXML);
            llReturn = true;


            return llReturn;
        }


        private Boolean upgradeRepo()
        {
            Boolean llReturn = false;
            string lcBatch = ServoyPath + @"\application_server\servoy_server.bat";

            // --- BatchDatei starten und auf Antwort warten ---
            var process = new Process();
            var startinfo = new ProcessStartInfo(lcBatch, "-upgradeRepository");
            startinfo.RedirectStandardOutput = true;
            startinfo.UseShellExecute = false;
            process.StartInfo = startinfo;
            process.OutputDataReceived += (sender, args) => Console.WriteLine(args.Data); // do whatever processing you need to do in this handler
            process.Start();
            process.BeginOutputReadLine();
            process.WaitForExit();


            return llReturn;
        }

        private void label6_Click(object sender, EventArgs e)
        {
            CreateConfig();
        }

        private void label7_Click(object sender, EventArgs e)
        {
            upgradeRepo();
        }
    }
}
