﻿namespace Updater2
{
    partial class UpdateMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(UpdateMain));
            this.panel1 = new System.Windows.Forms.Panel();
            this.cmdExit = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.cmdLog = new System.Windows.Forms.Button();
            this.cmdUpdate = new System.Windows.Forms.Button();
            this.panel3 = new System.Windows.Forms.Panel();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.pnlDesign = new System.Windows.Forms.Panel();
            this.pnlUpdate = new System.Windows.Forms.Panel();
            this.txtFileBox = new System.Windows.Forms.RichTextBox();
            this.label19 = new System.Windows.Forms.Label();
            this.lblDB = new System.Windows.Forms.Label();
            this.lblServoyPort = new System.Windows.Forms.Label();
            this.lblServoyPfad = new System.Windows.Forms.Label();
            this.lblServerURL = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.pnlPunkte = new System.Windows.Forms.Panel();
            this.pic10 = new System.Windows.Forms.PictureBox();
            this.pic9 = new System.Windows.Forms.PictureBox();
            this.pic8 = new System.Windows.Forms.PictureBox();
            this.pic6 = new System.Windows.Forms.PictureBox();
            this.pic7 = new System.Windows.Forms.PictureBox();
            this.pic5 = new System.Windows.Forms.PictureBox();
            this.pic4 = new System.Windows.Forms.PictureBox();
            this.pic3 = new System.Windows.Forms.PictureBox();
            this.pic2 = new System.Windows.Forms.PictureBox();
            this.pic1 = new System.Windows.Forms.PictureBox();
            this.cmdStartUpdate = new System.Windows.Forms.Button();
            this.label13 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.prgBar = new System.Windows.Forms.ProgressBar();
            this.pnlStatus = new System.Windows.Forms.Panel();
            this.lblStatus = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.pnlLog = new System.Windows.Forms.Panel();
            this.txtLog = new System.Windows.Forms.RichTextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.pic11 = new System.Windows.Forms.PictureBox();
            this.label20 = new System.Windows.Forms.Label();
            this.pic12 = new System.Windows.Forms.PictureBox();
            this.label21 = new System.Windows.Forms.Label();
            this.panel1.SuspendLayout();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.pnlUpdate.SuspendLayout();
            this.pnlPunkte.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pic10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic1)).BeginInit();
            this.pnlStatus.SuspendLayout();
            this.pnlLog.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pic11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic12)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.cmdExit);
            this.panel1.Controls.Add(this.button2);
            this.panel1.Controls.Add(this.button1);
            this.panel1.Controls.Add(this.cmdLog);
            this.panel1.Controls.Add(this.cmdUpdate);
            this.panel1.Controls.Add(this.panel3);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(188, 586);
            this.panel1.TabIndex = 0;
            // 
            // cmdExit
            // 
            this.cmdExit.FlatAppearance.BorderSize = 0;
            this.cmdExit.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdExit.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmdExit.ForeColor = System.Drawing.Color.White;
            this.cmdExit.Image = global::Updater2.Properties.Resources.Exit;
            this.cmdExit.Location = new System.Drawing.Point(0, 498);
            this.cmdExit.Name = "cmdExit";
            this.cmdExit.Size = new System.Drawing.Size(188, 83);
            this.cmdExit.TabIndex = 3;
            this.cmdExit.Text = "Exit";
            this.cmdExit.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.cmdExit.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.cmdExit.UseVisualStyleBackColor = true;
            this.cmdExit.Click += new System.EventHandler(this.cmdExit_Click);
            // 
            // button2
            // 
            this.button2.FlatAppearance.BorderSize = 0;
            this.button2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button2.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button2.ForeColor = System.Drawing.Color.White;
            this.button2.Image = global::Updater2.Properties.Resources.List;
            this.button2.Location = new System.Drawing.Point(4, 219);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(184, 73);
            this.button2.TabIndex = 2;
            this.button2.Text = "Log";
            this.button2.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.button2.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // button1
            // 
            this.button1.FlatAppearance.BorderSize = 0;
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button1.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.ForeColor = System.Drawing.Color.White;
            this.button1.Image = global::Updater2.Properties.Resources.Command_Line;
            this.button1.Location = new System.Drawing.Point(4, 138);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(184, 78);
            this.button1.TabIndex = 1;
            this.button1.Text = "Update";
            this.button1.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.button1.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.cmdUpdate_Click);
            // 
            // cmdLog
            // 
            this.cmdLog.FlatAppearance.BorderSize = 0;
            this.cmdLog.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdLog.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmdLog.ForeColor = System.Drawing.Color.White;
            this.cmdLog.Image = global::Updater2.Properties.Resources.List;
            this.cmdLog.Location = new System.Drawing.Point(0, 218);
            this.cmdLog.Name = "cmdLog";
            this.cmdLog.Size = new System.Drawing.Size(188, 73);
            this.cmdLog.TabIndex = 2;
            this.cmdLog.Text = "Log";
            this.cmdLog.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.cmdLog.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.cmdLog.UseVisualStyleBackColor = true;
            // 
            // cmdUpdate
            // 
            this.cmdUpdate.FlatAppearance.BorderSize = 0;
            this.cmdUpdate.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdUpdate.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmdUpdate.ForeColor = System.Drawing.Color.White;
            this.cmdUpdate.Image = global::Updater2.Properties.Resources.Command_Line;
            this.cmdUpdate.Location = new System.Drawing.Point(0, 136);
            this.cmdUpdate.Name = "cmdUpdate";
            this.cmdUpdate.Size = new System.Drawing.Size(188, 78);
            this.cmdUpdate.TabIndex = 1;
            this.cmdUpdate.Text = "Update";
            this.cmdUpdate.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.cmdUpdate.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.cmdUpdate.UseVisualStyleBackColor = true;
            this.cmdUpdate.Click += new System.EventHandler(this.cmdUpdate_Click);
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.pictureBox1);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel3.Location = new System.Drawing.Point(0, 0);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(188, 131);
            this.panel3.TabIndex = 0;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pictureBox1.Image = global::Updater2.Properties.Resources.hvo2go_markenzeichen_rgb_verlauf;
            this.pictureBox1.InitialImage = global::Updater2.Properties.Resources.hvo2go_markenzeichen_4c_verlauf;
            this.pictureBox1.Location = new System.Drawing.Point(0, 0);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(188, 131);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            this.pictureBox1.MouseDown += new System.Windows.Forms.MouseEventHandler(this.UpdateMain_MouseDown);
            this.pictureBox1.MouseMove += new System.Windows.Forms.MouseEventHandler(this.UpdateMain_MouseMove);
            this.pictureBox1.MouseUp += new System.Windows.Forms.MouseEventHandler(this.UpdateMain_MouseUp);
            // 
            // pnlDesign
            // 
            this.pnlDesign.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.pnlDesign.Location = new System.Drawing.Point(192, 136);
            this.pnlDesign.Name = "pnlDesign";
            this.pnlDesign.Size = new System.Drawing.Size(7, 78);
            this.pnlDesign.TabIndex = 2;
            // 
            // pnlUpdate
            // 
            this.pnlUpdate.Controls.Add(this.txtFileBox);
            this.pnlUpdate.Controls.Add(this.label19);
            this.pnlUpdate.Controls.Add(this.lblDB);
            this.pnlUpdate.Controls.Add(this.lblServoyPort);
            this.pnlUpdate.Controls.Add(this.lblServoyPfad);
            this.pnlUpdate.Controls.Add(this.lblServerURL);
            this.pnlUpdate.Controls.Add(this.label18);
            this.pnlUpdate.Controls.Add(this.label17);
            this.pnlUpdate.Controls.Add(this.label16);
            this.pnlUpdate.Controls.Add(this.label15);
            this.pnlUpdate.Controls.Add(this.label14);
            this.pnlUpdate.Controls.Add(this.pnlPunkte);
            this.pnlUpdate.Controls.Add(this.prgBar);
            this.pnlUpdate.Controls.Add(this.pnlStatus);
            this.pnlUpdate.Location = new System.Drawing.Point(205, 0);
            this.pnlUpdate.Name = "pnlUpdate";
            this.pnlUpdate.Size = new System.Drawing.Size(749, 581);
            this.pnlUpdate.TabIndex = 3;
            this.pnlUpdate.Visible = false;
            // 
            // txtFileBox
            // 
            this.txtFileBox.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(45)))), ((int)(((byte)(48)))));
            this.txtFileBox.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtFileBox.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtFileBox.ForeColor = System.Drawing.Color.White;
            this.txtFileBox.Location = new System.Drawing.Point(336, 32);
            this.txtFileBox.Name = "txtFileBox";
            this.txtFileBox.ReadOnly = true;
            this.txtFileBox.Size = new System.Drawing.Size(215, 223);
            this.txtFileBox.TabIndex = 18;
            this.txtFileBox.Text = "";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.Location = new System.Drawing.Point(332, 9);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(130, 19);
            this.label19.TabIndex = 17;
            this.label19.Text = "Update Dateien";
            // 
            // lblDB
            // 
            this.lblDB.AutoSize = true;
            this.lblDB.Location = new System.Drawing.Point(120, 98);
            this.lblDB.Name = "lblDB";
            this.lblDB.Size = new System.Drawing.Size(64, 21);
            this.lblDB.TabIndex = 16;
            this.lblDB.Text = "FEHLER";
            this.lblDB.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblServoyPort
            // 
            this.lblServoyPort.AutoSize = true;
            this.lblServoyPort.Location = new System.Drawing.Point(120, 32);
            this.lblServoyPort.Name = "lblServoyPort";
            this.lblServoyPort.Size = new System.Drawing.Size(64, 21);
            this.lblServoyPort.TabIndex = 15;
            this.lblServoyPort.Text = "FEHLER";
            this.lblServoyPort.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblServoyPfad
            // 
            this.lblServoyPfad.AutoSize = true;
            this.lblServoyPfad.Location = new System.Drawing.Point(120, 54);
            this.lblServoyPfad.Name = "lblServoyPfad";
            this.lblServoyPfad.Size = new System.Drawing.Size(64, 21);
            this.lblServoyPfad.TabIndex = 14;
            this.lblServoyPfad.Text = "FEHLER";
            this.lblServoyPfad.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblServerURL
            // 
            this.lblServerURL.AutoSize = true;
            this.lblServerURL.Location = new System.Drawing.Point(120, 76);
            this.lblServerURL.Name = "lblServerURL";
            this.lblServerURL.Size = new System.Drawing.Size(64, 21);
            this.lblServerURL.TabIndex = 13;
            this.lblServerURL.Text = "FEHLER";
            this.lblServerURL.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(4, 98);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(104, 21);
            this.label18.TabIndex = 12;
            this.label18.Text = "Datenbank:";
            this.label18.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(4, 32);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(100, 21);
            this.label17.TabIndex = 11;
            this.label17.Text = "Servoy Port:";
            this.label17.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(4, 54);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(105, 21);
            this.label16.TabIndex = 10;
            this.label16.Text = "Servoy Pfad:";
            this.label16.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(4, 76);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(94, 21);
            this.label15.TabIndex = 9;
            this.label15.Text = "Server URL:";
            this.label15.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(4, 9);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(97, 19);
            this.label14.TabIndex = 3;
            this.label14.Text = "Information";
            // 
            // pnlPunkte
            // 
            this.pnlPunkte.Controls.Add(this.pic12);
            this.pnlPunkte.Controls.Add(this.label21);
            this.pnlPunkte.Controls.Add(this.pic11);
            this.pnlPunkte.Controls.Add(this.label20);
            this.pnlPunkte.Controls.Add(this.pic10);
            this.pnlPunkte.Controls.Add(this.pic9);
            this.pnlPunkte.Controls.Add(this.pic8);
            this.pnlPunkte.Controls.Add(this.pic6);
            this.pnlPunkte.Controls.Add(this.pic7);
            this.pnlPunkte.Controls.Add(this.pic5);
            this.pnlPunkte.Controls.Add(this.pic4);
            this.pnlPunkte.Controls.Add(this.pic3);
            this.pnlPunkte.Controls.Add(this.pic2);
            this.pnlPunkte.Controls.Add(this.pic1);
            this.pnlPunkte.Controls.Add(this.cmdStartUpdate);
            this.pnlPunkte.Controls.Add(this.label13);
            this.pnlPunkte.Controls.Add(this.label12);
            this.pnlPunkte.Controls.Add(this.label11);
            this.pnlPunkte.Controls.Add(this.label9);
            this.pnlPunkte.Controls.Add(this.label8);
            this.pnlPunkte.Controls.Add(this.label7);
            this.pnlPunkte.Controls.Add(this.label6);
            this.pnlPunkte.Controls.Add(this.label5);
            this.pnlPunkte.Controls.Add(this.label4);
            this.pnlPunkte.Controls.Add(this.label2);
            this.pnlPunkte.Controls.Add(this.label3);
            this.pnlPunkte.Dock = System.Windows.Forms.DockStyle.Right;
            this.pnlPunkte.Location = new System.Drawing.Point(565, 0);
            this.pnlPunkte.Name = "pnlPunkte";
            this.pnlPunkte.Size = new System.Drawing.Size(184, 538);
            this.pnlPunkte.TabIndex = 2;
            // 
            // pic10
            // 
            this.pic10.Image = global::Updater2.Properties.Resources.Checked_16x16;
            this.pic10.Location = new System.Drawing.Point(10, 286);
            this.pic10.Name = "pic10";
            this.pic10.Size = new System.Drawing.Size(16, 16);
            this.pic10.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pic10.TabIndex = 21;
            this.pic10.TabStop = false;
            this.pic10.Visible = false;
            // 
            // pic9
            // 
            this.pic9.Image = global::Updater2.Properties.Resources.Checked_16x16;
            this.pic9.Location = new System.Drawing.Point(10, 264);
            this.pic9.Name = "pic9";
            this.pic9.Size = new System.Drawing.Size(16, 16);
            this.pic9.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pic9.TabIndex = 20;
            this.pic9.TabStop = false;
            this.pic9.Visible = false;
            // 
            // pic8
            // 
            this.pic8.Image = global::Updater2.Properties.Resources.Checked_16x16;
            this.pic8.Location = new System.Drawing.Point(10, 242);
            this.pic8.Name = "pic8";
            this.pic8.Size = new System.Drawing.Size(16, 16);
            this.pic8.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pic8.TabIndex = 19;
            this.pic8.TabStop = false;
            this.pic8.Visible = false;
            // 
            // pic6
            // 
            this.pic6.Image = global::Updater2.Properties.Resources.Checked_16x16;
            this.pic6.Location = new System.Drawing.Point(10, 154);
            this.pic6.Name = "pic6";
            this.pic6.Size = new System.Drawing.Size(16, 16);
            this.pic6.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pic6.TabIndex = 18;
            this.pic6.TabStop = false;
            this.pic6.Visible = false;
            // 
            // pic7
            // 
            this.pic7.Image = global::Updater2.Properties.Resources.Checked_16x16;
            this.pic7.Location = new System.Drawing.Point(10, 176);
            this.pic7.Name = "pic7";
            this.pic7.Size = new System.Drawing.Size(16, 16);
            this.pic7.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pic7.TabIndex = 17;
            this.pic7.TabStop = false;
            this.pic7.Visible = false;
            // 
            // pic5
            // 
            this.pic5.Image = global::Updater2.Properties.Resources.Checked_16x16;
            this.pic5.Location = new System.Drawing.Point(10, 132);
            this.pic5.Name = "pic5";
            this.pic5.Size = new System.Drawing.Size(16, 16);
            this.pic5.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pic5.TabIndex = 16;
            this.pic5.TabStop = false;
            this.pic5.Visible = false;
            // 
            // pic4
            // 
            this.pic4.Image = global::Updater2.Properties.Resources.Checked_16x16;
            this.pic4.Location = new System.Drawing.Point(10, 110);
            this.pic4.Name = "pic4";
            this.pic4.Size = new System.Drawing.Size(16, 16);
            this.pic4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pic4.TabIndex = 15;
            this.pic4.TabStop = false;
            this.pic4.Visible = false;
            // 
            // pic3
            // 
            this.pic3.Image = global::Updater2.Properties.Resources.Checked_16x16;
            this.pic3.Location = new System.Drawing.Point(10, 88);
            this.pic3.Name = "pic3";
            this.pic3.Size = new System.Drawing.Size(16, 16);
            this.pic3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pic3.TabIndex = 14;
            this.pic3.TabStop = false;
            this.pic3.Visible = false;
            // 
            // pic2
            // 
            this.pic2.Image = global::Updater2.Properties.Resources.Checked_16x16;
            this.pic2.Location = new System.Drawing.Point(10, 66);
            this.pic2.Name = "pic2";
            this.pic2.Size = new System.Drawing.Size(16, 16);
            this.pic2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pic2.TabIndex = 13;
            this.pic2.TabStop = false;
            this.pic2.Visible = false;
            // 
            // pic1
            // 
            this.pic1.Image = global::Updater2.Properties.Resources.Checked_16x16;
            this.pic1.Location = new System.Drawing.Point(10, 44);
            this.pic1.Name = "pic1";
            this.pic1.Size = new System.Drawing.Size(16, 16);
            this.pic1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pic1.TabIndex = 12;
            this.pic1.TabStop = false;
            this.pic1.Visible = false;
            // 
            // cmdStartUpdate
            // 
            this.cmdStartUpdate.Enabled = false;
            this.cmdStartUpdate.FlatAppearance.BorderSize = 0;
            this.cmdStartUpdate.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdStartUpdate.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmdStartUpdate.ForeColor = System.Drawing.Color.White;
            this.cmdStartUpdate.Image = global::Updater2.Properties.Resources.Go;
            this.cmdStartUpdate.Location = new System.Drawing.Point(0, 460);
            this.cmdStartUpdate.Name = "cmdStartUpdate";
            this.cmdStartUpdate.Size = new System.Drawing.Size(184, 78);
            this.cmdStartUpdate.TabIndex = 11;
            this.cmdStartUpdate.Text = "Starte Update";
            this.cmdStartUpdate.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.cmdStartUpdate.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.cmdStartUpdate.UseVisualStyleBackColor = true;
            this.cmdStartUpdate.Click += new System.EventHandler(this.cmdStartUpdate_Click);
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(62, 86);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(111, 21);
            this.label13.TabIndex = 10;
            this.label13.Text = "Pfade prüfen";
            this.label13.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(60, 64);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(113, 21);
            this.label12.TabIndex = 9;
            this.label12.Text = "Server prüfen";
            this.label12.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(47, 42);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(126, 21);
            this.label11.TabIndex = 8;
            this.label11.Text = "Update prüfen";
            this.label11.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(32, 284);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(141, 21);
            this.label9.TabIndex = 7;
            this.label9.Text = "HVO2go Update";
            this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(55, 262);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(118, 21);
            this.label8.TabIndex = 6;
            this.label8.Text = "Dienst starten";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(48, 240);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(125, 21);
            this.label7.TabIndex = 5;
            this.label7.Text = "Repo Upgrade";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.label7.Click += new System.EventHandler(this.label7_Click);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(43, 174);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(130, 21);
            this.label6.TabIndex = 4;
            this.label6.Text = "Config erstellen";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.label6.Click += new System.EventHandler(this.label6_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(46, 152);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(127, 21);
            this.label5.TabIndex = 3;
            this.label5.Text = "Server anlegen";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(49, 130);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(124, 21);
            this.label4.TabIndex = 2;
            this.label4.Text = "Servoy Backup";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(39, 108);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(134, 21);
            this.label2.TabIndex = 1;
            this.label2.Text = "Dienst anhalten";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(3, 9);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(173, 19);
            this.label3.TabIndex = 0;
            this.label3.Text = "Update 2020 - Punkte";
            // 
            // prgBar
            // 
            this.prgBar.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.prgBar.Location = new System.Drawing.Point(4, 515);
            this.prgBar.Name = "prgBar";
            this.prgBar.Size = new System.Drawing.Size(555, 23);
            this.prgBar.TabIndex = 1;
            // 
            // pnlStatus
            // 
            this.pnlStatus.Controls.Add(this.lblStatus);
            this.pnlStatus.Controls.Add(this.label1);
            this.pnlStatus.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pnlStatus.Location = new System.Drawing.Point(0, 538);
            this.pnlStatus.Name = "pnlStatus";
            this.pnlStatus.Size = new System.Drawing.Size(749, 43);
            this.pnlStatus.TabIndex = 0;
            // 
            // lblStatus
            // 
            this.lblStatus.AutoSize = true;
            this.lblStatus.Dock = System.Windows.Forms.DockStyle.Top;
            this.lblStatus.Location = new System.Drawing.Point(0, 19);
            this.lblStatus.Name = "lblStatus";
            this.lblStatus.Size = new System.Drawing.Size(133, 21);
            this.lblStatus.TabIndex = 1;
            this.lblStatus.Text = "TEXT WILL FORM";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Dock = System.Windows.Forms.DockStyle.Top;
            this.label1.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(0, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(56, 19);
            this.label1.TabIndex = 0;
            this.label1.Text = "Status:";
            // 
            // pnlLog
            // 
            this.pnlLog.Controls.Add(this.txtLog);
            this.pnlLog.Controls.Add(this.label10);
            this.pnlLog.Location = new System.Drawing.Point(200, 1);
            this.pnlLog.Name = "pnlLog";
            this.pnlLog.Size = new System.Drawing.Size(746, 578);
            this.pnlLog.TabIndex = 4;
            this.pnlLog.Visible = false;
            // 
            // txtLog
            // 
            this.txtLog.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(45)))), ((int)(((byte)(48)))));
            this.txtLog.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtLog.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtLog.ForeColor = System.Drawing.Color.White;
            this.txtLog.Location = new System.Drawing.Point(32, 60);
            this.txtLog.Name = "txtLog";
            this.txtLog.ReadOnly = true;
            this.txtLog.Size = new System.Drawing.Size(665, 475);
            this.txtLog.TabIndex = 2;
            this.txtLog.Text = "";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(30, 38);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(103, 19);
            this.label10.TabIndex = 1;
            this.label10.Text = "Updater Log";
            // 
            // pic11
            // 
            this.pic11.Image = global::Updater2.Properties.Resources.Checked_16x16;
            this.pic11.Location = new System.Drawing.Point(10, 198);
            this.pic11.Name = "pic11";
            this.pic11.Size = new System.Drawing.Size(16, 16);
            this.pic11.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pic11.TabIndex = 23;
            this.pic11.TabStop = false;
            this.pic11.Visible = false;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(39, 196);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(134, 21);
            this.label20.TabIndex = 22;
            this.label20.Text = "Service erstellen";
            this.label20.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // pic12
            // 
            this.pic12.Image = global::Updater2.Properties.Resources.Checked_16x16;
            this.pic12.Location = new System.Drawing.Point(10, 220);
            this.pic12.Name = "pic12";
            this.pic12.Size = new System.Drawing.Size(16, 16);
            this.pic12.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pic12.TabIndex = 25;
            this.pic12.TabStop = false;
            this.pic12.Visible = false;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(47, 218);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(126, 21);
            this.label21.TabIndex = 24;
            this.label21.Text = "Server erstellen";
            this.label21.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // UpdateMain
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(41)))), ((int)(((byte)(44)))), ((int)(((byte)(51)))));
            this.ClientSize = new System.Drawing.Size(964, 586);
            this.ControlBox = false;
            this.Controls.Add(this.pnlUpdate);
            this.Controls.Add(this.pnlDesign);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.pnlLog);
            this.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(62)))), ((int)(((byte)(120)))), ((int)(((byte)(138)))));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "UpdateMain";
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Show;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Updater 2020";
            this.Load += new System.EventHandler(this.UpdateMain_Load);
            this.panel1.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.pnlUpdate.ResumeLayout(false);
            this.pnlUpdate.PerformLayout();
            this.pnlPunkte.ResumeLayout(false);
            this.pnlPunkte.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pic10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic1)).EndInit();
            this.pnlStatus.ResumeLayout(false);
            this.pnlStatus.PerformLayout();
            this.pnlLog.ResumeLayout(false);
            this.pnlLog.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pic11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic12)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Button cmdUpdate;
        private System.Windows.Forms.Button cmdExit;
        private System.Windows.Forms.Button cmdLog;
        private System.Windows.Forms.Panel pnlDesign;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Panel pnlUpdate;
        private System.Windows.Forms.Panel pnlStatus;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lblStatus;
        private System.Windows.Forms.ProgressBar prgBar;
        private System.Windows.Forms.Panel pnlPunkte;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Panel pnlLog;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.RichTextBox txtLog;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label lblDB;
        private System.Windows.Forms.Label lblServoyPort;
        private System.Windows.Forms.Label lblServoyPfad;
        private System.Windows.Forms.Label lblServerURL;
        private System.Windows.Forms.Button cmdStartUpdate;
        private System.Windows.Forms.PictureBox pic10;
        private System.Windows.Forms.PictureBox pic9;
        private System.Windows.Forms.PictureBox pic8;
        private System.Windows.Forms.PictureBox pic6;
        private System.Windows.Forms.PictureBox pic7;
        private System.Windows.Forms.PictureBox pic5;
        private System.Windows.Forms.PictureBox pic4;
        private System.Windows.Forms.PictureBox pic3;
        private System.Windows.Forms.PictureBox pic2;
        private System.Windows.Forms.PictureBox pic1;
        private System.Windows.Forms.RichTextBox txtFileBox;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.PictureBox pic12;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.PictureBox pic11;
        private System.Windows.Forms.Label label20;
    }
}