﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Updater2
{
    static class Program
    {
        /// <summary>
        /// Der Haupteinstiegspunkt für die Anwendung.
        /// </summary>
        [STAThread]
        static void Main(string[] args)
        {
            if (args.Length > 0 && args[0] == "-s")             // SilentUpdate ausführen wenn Argument stimmt 
            {
                Globals.Silent = true;
            }
            else
            {
                Globals.Silent = false;
            }

            if (System.Diagnostics.Debugger.IsAttached)         // Prüft ob es Produktiv oder im Debugger läuft und setzt den HOST
            {
                Globals.Host = "http://test02";
            }
            else
            {
                Globals.Host = "http://localhost";
            }

            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new UpdateMain());
        }
    }


    public class Globals
    {
        private static bool _silent;
        public static bool Silent
        {
            get
            {
                return _silent;
            }
            set
            {
                _silent = value;
            }
        }
        private static string _host;
        public static string Host 
        {
            get
            {
                return _host;
            }
            set
            {
                _host = value;
            }
        }
        private static string _port;
        public static string Port
        {
            get
            {
                return _port;
            }
            set
            {
                _port = value;
            }
        }

        public static string InstallPath    = @"C:\ServoyUpdate\";
        public static string VersionsInfo   = "HVO2go Updater\nVersion: 2.0\nBuild: 903\nDatum: 09.04.2020 13:45\n~VL";      // Bei jedem Update bitte Build, Datum und Programmer anpassen
    }
}

